import requests
from common.config import telegram_api_key
import telebot
import time


def send_message(secret_token_id, message_to_send, chat_id):
    send_message_url = "https://api.telegram.org/bot" + secret_token_id + "/sendMessage?chat_id={}&text={}".format(
        chat_id, message_to_send)
    if message_to_send is not None or message_to_send is not "":
        response = requests.get(send_message_url)
        response = response.json()
        if response['ok']:
            print(f'[SUCCESS] Telegram message "{message_to_send}" was sent')
        else:
            print(f'[WARNING] Telegram message was not sent')
    else:
        print(f'[WEIRD] Telegram message was not found')


def testing():
    bot = telebot.TeleBot(telegram_api_key)

    @bot.message_handler(content_types=['text'])
    def handle_command_adminwindow(message):
        print("User reply: ", message)
        bot.send_message(chat_id=message.chat.id,
                         text="How are you?",
                         parse_mode='HTML')
        # send_message(telegram_api_key, 'TESTING AUTO', 925797157)

    while True:
        try:
            # bot waits for default time before terminating
            bot.polling(none_stop=True, interval=0, timeout=0)
        except:
            time.sleep(10)


if __name__ == "__main__":
    testing()
    # send_message(telegram_api_key, "I'm good, thank you", 925797157)
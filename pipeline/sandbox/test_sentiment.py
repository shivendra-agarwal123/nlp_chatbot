import numpy as np
from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types


def extract_sentiment(message: str):
    """Run a sentiment analysis request on text ."""
    print("Text - ", message)
    client = language.LanguageServiceClient.from_service_account_json('common/DeepMakers-c4af70a7704c.json')
    document = types.Document(
        content=message,
        type=enums.Document.Type.PLAIN_TEXT)
    """results = client.analyze_sentiment(document=document)
    score = results.document_sentiment.score
    magnitude = results.document_sentiment.magnitude
    print("analyze_sentiment - score", score)
    print("analyze_sentiment - magnitude", magnitude)"""

    results = client.analyze_entities(document=document)
    print("analyze_entities - language", results.language)

    results = client.analyze_entity_sentiment(document=document)
    score = results.document_sentiment.magnitude
    magnitude = results.document_sentiment.magnitude
    print("analyze_entity_sentiment - score", score)
    print("analyze_entity_sentiment - magnitude", magnitude)

    results = client.analyze_syntax(document=document)
    score = results.document_sentiment.magnitude
    magnitude = results.document_sentiment.magnitude
    print("analyze_syntax - score", score)
    print("analyze_syntax - magnitude", magnitude)
    return score, magnitude


if __name__ == "__main__":
    extract_sentiment("he was too bad")
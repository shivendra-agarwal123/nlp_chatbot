import pymongo
from pymongo import UpdateOne, DeleteOne
from pymongo.errors import BulkWriteError, ConfigurationError, InvalidName, CollectionInvalid, ConnectionFailure, \
    InvalidOperation
from common.config_db import config_db
from common.config_db import collection_names


def init_db(test_db=None):
    try:
        client = pymongo.MongoClient(config_db.db_address)
        if test_db is not None:
            mydb = client[test_db]
        else:
            mydb = client[config_db.db_name]
        collection_dict = {
            collection_names.active: mydb[collection_names.active],
            collection_names.pending: mydb[collection_names.pending],
            collection_names.completed: mydb[collection_names.completed],
            collection_names.leads: mydb[collection_names.leads],
            collection_names.abandoned: mydb[collection_names.abandoned]
        }
        return collection_dict
    except ConfigurationError or InvalidName or CollectionInvalid or ConnectionFailure:
        raise ConfigurationError(f"{config_db.db_name} or {config_db.db_address} or a collection not found")


def get_db():
    # Init DB and return all collections
    collections_dict = init_db()

    active_collection = collections_dict.get(collection_names.active)
    pending_collection = collections_dict.get(collection_names.pending)
    completed_collection = collections_dict.get(collection_names.completed)
    leads_collection = collections_dict.get(collection_names.leads)
    abandoned_collection = collections_dict.get(collection_names.abandoned)
    return collections_dict, active_collection, pending_collection, completed_collection, leads_collection, abandoned_collection
